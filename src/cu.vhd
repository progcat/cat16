library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cu is
    generic (n: integer := 16);
    port(
        inst_in: in std_logic_vector(n-1 downto 0);
        clk, rst: in std_logic
    );
end entity cu;

architecture rtl of cu is
    type State is ();
    signal inst: std_logic_vector(n-1 downto 0);
    signal ir_en: std_logic;

begin
    inst_reg: entity work.reg port map (inst_in, inst, clk, ir_en);
    process(clk, rst)
    begin
        if rising_edge(rst) then
            -- reset counters
            --reset registers
        elsif rising_edge(clk) then
            
        end if;
    end process;
    
end architecture rtl;