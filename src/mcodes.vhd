library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

type mcode is record
    -- control lines
    -- * registers
    -- * alu
    -- * data bus
end record;

type opcode is record
    t1, t2, t3: mcode;
end record;

type opcode_table is array (natural range<>) of opcode;

constant opcode_decoder: opcode_table := (
    -- 0x0: NOP
    (),
    -- 0x1: IN Rx
    (),
    -- 0x2: MOV Rx, Ry
    (),
    -- 0x3: MOVZ Rx, Ry
    (),
    -- 0x4: MOVNZ Rx, Ry
    (),
    -- 0x5: OR Rx, Ry
    (),
    -- 0x6: AND Rx, Ry
    (),
    -- 0x7: NOT Rx
    (),
    -- 0x8: XOR Rx, Ry
    (),
    -- 0x9: XNOR Rx, Ry
    (),
    -- 0xa: NOR Rx, Ry
    (),
    -- 0xb: NAND Rx, Ry
    (),
    -- 0xc: ADD Rx, Ry
    (),
    -- 0xd: SUB Rx, Ry
    (),
);