library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity clock is
    generic(period: time := 10 us); -- 10us per cycle by default
    port (
        clk: out std_logic
    );
end entity clock;

architecture rtl of clock is
begin
    process
    begin
        clk <= '0';
        wait for period / 2;
        clk <= '1';
        wait for period / 2;
    end process;
end architecture rtl;