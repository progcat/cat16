library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity reg is
    generic (n: integer := 16);
    port (
        D: in std_logic_vector(n-1 downto 0);
        Q: out std_logic_vector(n-1 downto 0);
        clk, en, rst: in std_logic
    );
end entity;

architecture rtl of reg is
    
begin
    process(clk)
    begin
        -- Reset
        if rising_edge(rst) then
            for i in Q'range loop
                Q(i) <= '0';
            end loop;
        elsif rising_edge(clk) then
            -- Set
            if en = '1' then
                Q <= D;
            end if;
        end if;
    end process;
end architecture;