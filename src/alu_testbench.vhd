library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity testbench is
end entity testbench;

architecture rtl of testbench is
    signal inp_a, inp_b, alu_oup, oup: std_logic_vector(15 downto 0);
    signal mux_sig: std_logic_vector(1 downto 0);
    signal cin, a_en, b_en, inv_a, inv_b, inv_out, sum_en, cout_en, cin_en, cout: std_logic;
    type DataStruct is record
        INP_A, INP_B, OUP: std_logic_vector(15 downto 0);
        MUX_SIG: std_logic_vector(1 downto 0);
        CIN, A_EN, B_EN, INV_A, INV_B, INV_OUT, SUM_EN, COUT_EN, CIN_EN, COUT: std_logic;
    end record;

    type TestData is array (natural range <>) of DataStruct;
    constant test_data: TestData := (
        -- A        B        OUP    mux   cin  a_en b_en    inv_a inv_b  inv_o sum_en cout_en cin_en  cout
        (x"f0f0", x"0000", x"f0f0", "00", '0', '1',  '0',   '0',  '0',   '0',  '1',   '0',    '0',    '0'), -- stright through
        (x"000a", x"0000", x"fff5", "00", '0', '1',  '0',   '1',  '0',   '0',  '1',   '0',    '0',    '0'), -- not A
        (x"0000", x"000a", x"fff5", "00", '0', '0',  '1',   '0',  '1',   '0',  '1',   '0',    '0',    '1'), -- not B
        (x"0001", x"0001", x"0002", "00", '0', '1',  '1',   '0',  '0',   '0',  '1',   '0',    '1',    '0'), -- add
        (x"0005", x"0002", x"0003", "00", '1', '1',  '1',   '0',  '1',   '0',  '1',   '0',    '1',    '1'), -- sub
        (x"0005", x"000a", x"000f", "00", '0', '1',  '1',   '0',  '0',   '0',  '1',   '1',    '0',    '0'), -- or
        (x"000a", x"0002", x"0002", "00", '0', '1',  '1',   '0',  '0',   '0',  '0',   '1',    '0',    '0'), -- and
        (x"000a", x"0009", x"0003", "00", '0', '1',  '1',   '0',  '0',   '0',  '1',   '0',    '0',    '0'), -- xor
        (x"000a", x"0009", x"fffc", "00", '0', '1',  '1',   '0',  '0',   '1',  '1',   '0',    '0',    '0'), -- xnor
        (x"0005", x"000a", x"fff0", "00", '0', '1',  '1',   '0',  '0',   '1',  '1',   '1',    '0',    '0'), -- nor
        (x"000a", x"0002", x"fffd", "00", '0', '1',  '1',   '0',  '0',   '1',  '0',   '1',    '0',    '0'), -- nand
        (x"5555", x"0000", x"aaaa", "01", '0', '1',  '0',   '0',  '0',   '0',  '1',   '0',    '0',    '0'), -- shl
        (x"aaaa", x"0000", x"5555", "10", '0', '1',  '0',   '0',  '0',   '0',  '1',   '0',    '0',    '0') --shr
    );
begin
    alu: entity work.alu port map(inp_a, inp_b, cin, a_en, b_en, inv_a, inv_b, inv_out, sum_en, cout_en, cin_en, cout, alu_oup);
    shift_mux: entity work.shift_mux port map(alu_oup, mux_sig, oup);
    process
    begin
        for i in test_data'range loop
            inp_a <= test_data(i).INP_A;
            inp_b <= test_data(i).INP_B;
            mux_sig <= test_data(i).MUX_SIG;
            cin <= test_data(i).CIN;
            a_en <= test_data(i).A_EN;
            b_en <= test_data(i).B_EN;
            inv_a <= test_data(i).INV_A;
            inv_b <= test_data(i).INV_B;
            inv_out <= test_data(i).INV_OUT;
            sum_en <= test_data(i).SUM_EN;
            cout_en <= test_data(i).COUT_EN;
            cin_en <= test_data(i).CIN_EN;

            -- check ouput
            wait for 1 us;
            assert oup = test_data(i).OUP report "Wrong output" severity error;
            assert cout = test_data(i).COUT report "Wrong cout" severity error;
        end loop;
        wait;
    end process;    
end architecture rtl;