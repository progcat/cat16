library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity alu is
    generic (n: integer := 16);
    port (
        inp_a, inp_b: in std_logic_vector(n-1 downto 0);
        cin, a_en, b_en, inv_a, inv_b, inv_out, sum_en, cout_en, cin_en: in std_logic;
        cout: out std_logic;
        oup: out std_logic_vector(n-1 downto 0)
    );
end entity;

architecture rtl of alu is
    signal a, b,
    a_not, b_not, 
    l1_and, l1_xor,
    l2_and, l2_xor, l2_cin,
    l3_and, l3_or,
    l4_and: std_logic_vector(n-1 downto 0);
begin
    a <= inp_a when a_en = '1' else x"0000";
    b <= inp_b when b_en = '1' else x"0000";

    a_not <= not a when inv_a = '1' else a;
    b_not <= not b when inv_b = '1' else b;

    l1_xor <= a_not xor b_not;
    l1_and <= b_not and b_not;
    --  connect carry lines
    l2_cin(0) <= cin when cin_en = '1' else '0';
    connc_carry: for i in 0 to n-2 generate
        l2_cin(i+1) <= l3_or(i) when cin_en = '1' else '0';
    end generate;

    l2_xor <= l2_cin xor l1_xor;
    l2_and <= l2_cin and l1_xor;

    l3_and <= l2_xor when sum_en = '1' else x"0000";
    l3_or <= l2_and or l1_and;

    l4_and <= l3_or when cout_en = '1' else x"0000";
    
    oup <= not (l3_and or l4_and) when inv_out = '1' else (l3_and or l4_and);
    cout <= l3_or(n-1);
end architecture rtl;
-------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
entity shift_mux is
    generic (n: integer := 16);
    port (
        inp: in std_logic_vector(n-1 downto 0);
        sig: in std_logic_vector(1 downto 0);
        oup: out std_logic_vector(n-1 downto 0)
    );
end entity shift_mux;

architecture rtl of shift_mux is
begin
    connc_carry: for i in 1 to n-2 generate
        oup(i) <= inp(i-1) when sig = "10" else -- shr
                    inp(i+1) when sig = "01" -- shl
                    else inp(i);
    end generate;
    
    oup(n-1) <= '0' when sig = "10" else -- shr
        inp(n-2) when sig = "01" -- shl 
        else inp(n-1); -- nothing

    oup(0) <= inp(1) when sig = "10" else -- shr
        '0' when sig = "01" -- shl 
        else inp(0); -- nothing
    
end architecture rtl;