library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity testbench is
end entity testbench;

architecture rtl of testbench is
    signal d, q: std_logic_vector(3 downto 0);
    signal clk, en, rst: std_logic;
    
    type TestData is record
        D: std_logic_vector(3 downto 0);
        EN: std_logic;
        Q: std_logic_vector(3 downto 0);
        RST: std_logic;
    end record TestData;
    type TestBatch is array (natural range <>) of TestData;
    constant batches: TestBatch := (
        (x"0", '0', "UUUU", '0'),
        (x"0", '1', x"0", '0'),
        (x"5", '0', x"0", '0'),
        (x"5", '1', x"5", '0'),
        (x"0", '0', x"0", '1') -- Reset test
    );
begin
    reg: entity work.reg generic map(4) port map (d, q, clk, en, rst);
    process
    begin
        clk <= '0';
        for i in batches'range loop
            d <= batches(i).D;
            en <= batches(i).EN;
            rst <= batches(i).RST;
            wait for 1 us;
            clk <= '1';
            wait for 1 us;
            assert q = batches(i).Q report "Wrong output" severity error; 
            clk <= '0';
        end loop;
        wait;
    end process;    
end architecture rtl;