library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity testbench is
end entity testbench;

architecture rtl of testbench is
    signal clk_sig: std_logic;
begin
    clk: entity work.clock port map (clk_sig);
    process
    begin
        wait;
    end process;    
end architecture rtl;